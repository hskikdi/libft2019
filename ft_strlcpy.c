/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   ft_strlcpy.c                                       :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: hskikdi <marvin@42.fr>                     +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2019/10/29 17:42:09 by hskikdi           #+#    #+#             */
/*   Updated: 2019/11/07 17:10:12 by hskikdi          ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "libft.h"

size_t		ft_strlcpy(char *dst, const char *src, size_t dstsize)
{
	size_t		i;
	size_t		len;

	len = ft_strlen(src);
	i = 0;
	if (!dstsize)
		return (len);
	while (src[i] && i < dstsize - 1 && i < len)
	{
		dst[i] = src[i];
		i++;
	}
	dst[i] = (char)0;
	return (len);
}

/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   ft_lstmap.c                                        :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: hskikdi <marvin@42.fr>                     +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2019/10/30 20:12:55 by hskikdi           #+#    #+#             */
/*   Updated: 2019/11/13 19:11:40 by hskikdi          ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "libft.h"

t_list	*ft_lstmap(t_list *lst, void *(*f)(void *), void (*del)(void *))
{
	t_list	*ret;
	t_list	*tmp;
	void	*to_del;

	if (lst)
		to_del = f(lst->content);
	if (!lst || !(tmp = ft_lstnew(to_del)))
	{
		del(to_del);
		return (NULL);
	}
	ret = tmp;
	lst = lst->next;
	while (lst)
	{
		to_del = f(lst->content);
		if (!(tmp->next = ft_lstnew(to_del)))
		{
			ft_lstclear(&ret, del);
			return (NULL);
		}
		tmp = tmp->next;
		lst = lst->next;
	}
	return (ret);
}

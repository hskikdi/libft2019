/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   ft_calloc.c                                        :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: hskikdi <marvin@42.fr>                     +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2019/10/22 18:25:09 by hskikdi           #+#    #+#             */
/*   Updated: 2019/11/05 16:32:43 by hskikdi          ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "libft.h"

void	*ft_calloc(size_t nelem, size_t size)
{
	void	*ret;

	if (!(ret = malloc(size * nelem)))
		return (ret);
	ft_bzero(ret, nelem * size);
	return (ret);
}

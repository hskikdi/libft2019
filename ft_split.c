/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   ft_split.c                                         :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: hskikdi <marvin@42.fr>                     +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2019/10/25 15:36:33 by hskikdi           #+#    #+#             */
/*   Updated: 2019/11/19 14:59:54 by hskikdi          ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "libft.h"

static char		**ft_tabfree(char **par)
{
	char	**to_free;

	to_free = par;
	while (par && *par)
	{
		free(*par);
		*par = NULL;
		par++;
	}
	free(to_free);
	return (NULL);
}

static int		ft_getword(char const *s, char c)
{
	int word;
	int find;

	word = 0;
	find = 0;
	while (*s)
	{
		if (find && *s == c)
			find = 0;
		if (!find && *s != c)
		{
			find = 1;
			word++;
		}
		s++;
	}
	return (word);
}

char			**ft_split(char const *s, char c)
{
	int		split;
	char	**tab;
	int		i;
	int		j;
	int		start;

	if (!s || (i = 0))
		return (NULL);
	split = ft_getword(s, c);
	if (!(tab = malloc((sizeof(char*) * (split + 1)))))
		return (NULL);
	j = 0;
	while (j < split)
	{
		while (s[i] && s[i] == c)
			i++;
		start = i;
		while (s[i] && s[i] != c)
			i++;
		if (!(tab[j++] = ft_substr(s, start, i - start)))
			return (ft_tabfree(tab));
		i++;
	}
	tab[j] = NULL;
	return (tab);
}

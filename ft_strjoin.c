/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   ft_strjoin.c                                       :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: hskikdi <marvin@42.fr>                     +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2019/10/22 19:00:06 by hskikdi           #+#    #+#             */
/*   Updated: 2019/11/07 17:26:24 by hskikdi          ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "libft.h"

static int		ft_strlen_safe(const char *s)
{
	int i;

	i = 0;
	while (s && s[i])
		i++;
	return (i);
}

char			*ft_strjoin(char const *s1, char const *s2)
{
	char	*ret;
	int		len;

	len = ft_strlen_safe(s1) + ft_strlen_safe(s2) + 1;
	if (!(ret = (char *)malloc(sizeof(char) * len)))
		return (NULL);
	*ret = '\0';
	if (s1)
		ft_strcat(ret, s1);
	if (s2)
		ft_strcat(ret, s2);
	return (ret);
}

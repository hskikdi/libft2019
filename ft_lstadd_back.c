/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   ft_lstadd_back.c                                   :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: hskikdi <marvin@42.fr>                     +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2019/10/30 14:52:12 by hskikdi           #+#    #+#             */
/*   Updated: 2019/11/07 17:20:44 by hskikdi          ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "libft.h"

void	ft_lstadd_back(t_list **alst, t_list *new)
{
	t_list		*tmp;

	tmp = *alst;
	if (!tmp)
	{
		*alst = new;
		return ;
	}
	if (!alst || !tmp || !new)
		return ;
	while (tmp->next)
		tmp = tmp->next;
	tmp->next = new;
	if (new && new->next)
		new->next = NULL;
}

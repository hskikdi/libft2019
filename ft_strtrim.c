/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   ft_strtrim.c                                       :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: hskikdi <marvin@42.fr>                     +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2019/10/23 15:49:51 by hskikdi           #+#    #+#             */
/*   Updated: 2019/11/18 19:03:14 by hskikdi          ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "libft.h"

static int		ft_strlen_safe(const char *s)
{
	int		i;

	i = 0;
	while (s && s[i])
		i++;
	return (i);
}

static	int		is_it_in_set(char c, const char *set)
{
	while (set && *set)
	{
		if (c == *set)
			return (1);
		set++;
	}
	return (0);
}

char			*ft_strtrim(const char *s, const char *set)
{
	int				i;
	size_t			k;

	i = 0;
	k = ft_strlen_safe(s) - 1;
	while (s && k > 0 && is_it_in_set(s[k], set))
		k--;
	while (s && is_it_in_set(s[i], set))
		i++;
	if (i == ft_strlen_safe(s))
		return (ft_strdup(""));
	return (ft_substr(s, i, k - i + 1));
}

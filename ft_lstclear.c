/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   ft_lstclear.c                                      :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: hskikdi <marvin@42.fr>                     +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2019/10/30 17:01:33 by hskikdi           #+#    #+#             */
/*   Updated: 2019/11/08 16:17:09 by hskikdi          ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "libft.h"

void	ft_lstclear(t_list **alst, void (*del)(void *))
{
	t_list	*tmp;

	if (!alst || !*alst || !del)
		return ;
	tmp = (*alst)->next;
	while (tmp)
	{
		tmp = (*alst)->next;
		ft_lstdelone(*alst, del);
		*alst = tmp;
	}
}

# **************************************************************************** #
#                                                                              #
#                                                         :::      ::::::::    #
#    Makefile                                           :+:      :+:    :+:    #
#                                                     +:+ +:+         +:+      #
#    By: hskikdi <marvin@42.fr>                     +#+  +:+       +#+         #
#                                                 +#+#+#+#+#+   +#+            #
#    Created: 2019/10/23 15:21:46 by hskikdi           #+#    #+#              #
#    Updated: 2019/11/07 17:16:34 by hskikdi          ###   ########.fr        #
#                                                                              #
# **************************************************************************** #

NAME = libft.a
OBJ = \
	  ft_atoi.o \
	  ft_bzero.o \
	  ft_calloc.o	\
	  ft_isalnum.o \
	  ft_isalpha.o \
	  ft_isascii.o \
	  ft_isdigit.o \
	  ft_isprint.o \
	  ft_memccpy.o \
	  ft_memchr.o \
	  ft_memcmp.o \
	  ft_memcpy.o \
	  ft_memmove.o \
	  ft_memset.o \
	  ft_putchar_fd.o \
	  ft_putendl_fd.o \
	  ft_putnbr_fd.o \
	  ft_putstr_fd.o \
	  ft_split.o \
	  ft_strcat.o \
	  ft_strchr.o \
	  ft_strcmp.o \
	  ft_strdup.o \
	  ft_strjoin.o \
	  ft_strlcat.o \
	  ft_strlcpy.o \
	  ft_strlen.o \
	  ft_strmapi.o \
	  ft_strncmp.o \
	  ft_strnstr.o \
	  ft_strrchr.o \
	  ft_strtrim.o \
	  ft_substr.o \
	  ft_tolower.o \
	  ft_toupper.o \
	  ft_itoa.o 

BONUS = \
	  ft_memdel.o \
	  ft_lstadd_back.o \
	  ft_lstadd_front.o \
	  ft_lstclear.o \
	  ft_lstnew.o \
	  ft_lstdelone.o \
	  ft_lstiter.o \
	  ft_lstlast.o \
	  ft_lstsize.o \
	  ft_lstmap.o 

FLAG = -Werror -Wextra -Wall

all: $(NAME)

$(NAME): $(OBJ)
	ar -rsc $(NAME) $(OBJ)

%.o:%.c
	gcc $(FLAG) -c $< -o $@

clean:
	rm -f $(OBJ) $(BONUS)

fclean: clean
	rm -f $(NAME)

re: fclean all

bonus: $(OBJ) $(BONUS)
	ar -rsc $(NAME) $(OBJ) $(BONUS)

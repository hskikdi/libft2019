/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   ft_strlcat.c                                       :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: hskikdi <marvin@42.fr>                     +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2019/10/22 17:39:49 by hskikdi           #+#    #+#             */
/*   Updated: 2019/10/22 17:39:50 by hskikdi          ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "libft.h"

size_t	ft_strlcat(char *dst, const char *src, size_t dstsize)
{
	int		i;
	size_t	k;
	int		len;

	i = 0;
	k = ft_strlen(dst);
	len = ft_strlen(src);
	if (dstsize < k)
		return (ft_strlen(src) + dstsize);
	while (src[i] && k + i < dstsize - 1)
	{
		dst[k + i] = src[i];
		i++;
	}
	dst[k + i] = '\0';
	return (len + k);
}

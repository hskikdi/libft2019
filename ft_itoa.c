/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   ft_itoa.c                                          :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: hskikdi <marvin@42.fr>                     +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2019/10/29 15:45:17 by hskikdi           #+#    #+#             */
/*   Updated: 2019/11/13 17:03:50 by hskikdi          ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "libft.h"

static int		size_nbr(int nbr)
{
	int ret;

	ret = (nbr > 0) ? 0 : 1;
	while (nbr)
	{
		nbr = nbr / 10;
		ret++;
	}
	return (ret);
}

static void		core_itoa(char *str, unsigned int n, int *index)
{
	if (n >= 10)
		core_itoa(str, n / 10, index);
	str[(*index)++] = n % 10 + '0';
}

char			*ft_itoa(int nbr)
{
	char			*ret;
	unsigned int	n;
	int				size;
	int				i;

	i = 0;
	n = (nbr >= 0) ? nbr : -nbr;
	size = size_nbr(nbr);
	if (!(ret = (char*)malloc(sizeof(char) * (size + 1))))
		return (ret);
	if (nbr < 0)
		ret[i++] = '-';
	core_itoa(ret, n, &i);
	ret[i] = (char)0;
	return (ret);
}
